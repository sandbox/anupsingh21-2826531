<?php

/**
 * @file
 * Deals with the product details.
 */

/**
 * Product details of orders in track order page.
 */
function commerce_track_order_menu_item_redirect($order) {
  $skin = variable_get('commerce_track_order_skin', 'commerce_track_order');
  // Loading it as CSS_THEME to override the default style css properties.
  drupal_add_css(drupal_get_path('module', 'commerce_track_order') . '/css/commerce_track_order.css');
  $status_line_items = array();
  foreach ($order->commerce_line_items as $line_items) {
    foreach ($line_items as $line_item) {
      $status = commerce_track_order_all_order_status();
      $status_bar = '';
      $class_array = array('old-status', 'current-status', 'pending-status');
      empty($order->field_order_status[LANGUAGE_NONE][0]['tid']) ? $current = 2 : $current = 0;
      foreach ($status as $state) {
        if (!empty($order->field_order_status) && ($state->tid == $order->field_order_status[LANGUAGE_NONE][0]['tid'])) {
            $current++;
            $status_bar .= "<span class = '" . $class_array[$current] . "'>" . $state->name . "</span>";
            $current++;
            continue;
        }
        $status_bar .= "<span class = '" . $class_array[$current] . "'>" . $state->name . "</span>";
      }
      $status_line_items[$line_item['line_item_id']] = $status_bar;
    }
  }
  return theme($skin, array('order' => $order, 'status' => $status_line_items));

}

<?php

/**
 * @file
 * Displays track order page with grid style.
 */
?>
<div class="section group">
<div class="col span_1_of_2">
<div style="overflow-x:auto;">
  <table>
    <tr>
      <th>TITLE</th>
      <th>PRICE</th>
      <th>QTY</th>
      <th>TOTAL</th>
    </tr>
<?php foreach ($order->commerce_line_items[LANGUAGE_NONE] as $delta => $item): ?>
<?php $line_item = commerce_line_item_load($item['line_item_id']);?>

<tr>
      <td><?php print $line_item->line_item_label;?></td>
      <td><?php print commerce_currency_format($line_item->commerce_unit_price[LANGUAGE_NONE][0]['amount'], $line_item->commerce_unit_price[LANGUAGE_NONE][0]['currency_code'])?></td>
      <td><?php print $line_item->quantity;?></td>
      <td><?php print commerce_currency_format($line_item->commerce_total[LANGUAGE_NONE][0]['amount'], $line_item->commerce_unit_price[LANGUAGE_NONE][0]['currency_code']); ?></td>
    </tr>

<?php endforeach; ?>
 </table>
</div>
</div>
<div class="col span_1_of_2">
  <?php print $status[$item['line_item_id']]; ?>
..</div>
</div>

<?php
  $track_order_output = field_view_field('commerce_order', $order, 'commerce_customer_billing');
  print render($track_order_output);
?>

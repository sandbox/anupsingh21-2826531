<?php

/**
 * @file
 * Displays track order page with interactive style.
 */
?>
<div class="table interactive" style="overflow-x:auto;">
  <div class="row header blue">
    <div class="cell">
      TITLE
    </div>
    <div class="cell">
      PRICE
    </div>
    <div class="cell">
      QTY
    </div>
    <div class="cell">
      TOTAL
    </div>
  </div>
  <?php foreach ($order->commerce_line_items[LANGUAGE_NONE] as $delta => $item): ?>
  <?php $line_item = commerce_line_item_load($item['line_item_id']);?>
  <div class="row">
    <div class="cell">
      <?php print $line_item->line_item_label;?>
    </div>
    <div class="cell">
      <?php print commerce_currency_format($line_item->commerce_unit_price[LANGUAGE_NONE][0]['amount'], $line_item->commerce_unit_price[LANGUAGE_NONE][0]['currency_code'])?>
    </div>
    <div class="cell">
      <?php print $line_item->quantity;?>
    </div>
    <div class="cell">
      <?php print commerce_currency_format($line_item->commerce_total[LANGUAGE_NONE][0]['amount'], $line_item->commerce_unit_price[LANGUAGE_NONE][0]['currency_code']); ?>
    </div>
  </div>
  <?php endforeach;?>
</div>
<div class="section group tab2">
  <div class="col span_1_of_2">
    <?php
      $track_order_output = field_view_field('commerce_order', $order, 'commerce_customer_billing');
      print render($track_order_output);
      ?>
  </div>
  <div class="col span_1_of_2 tstatus">
    <?php print $status[$item['line_item_id']]; ?>
  </div>
</div>

<?php

/**
 * @file
 * Displays track order page with normal style.
 */
?>
<div class="content">
    <table class="sticky-header" style="position: fixed; top: 29px; left: 40px; visibility: hidden;"><thead style=""><tr><th>Title</th><th>Price</th><th>QTY</th><th>Total</th><th>Status</th> </tr></thead></table><table class="sticky-enabled tableheader-processed sticky-table">
 <thead><tr><th>Title</th><th>Price</th><th>QTY</th><th>Total</th><th>Status</th> </tr></thead>
<tbody>
<?php foreach ($order->commerce_line_items[LANGUAGE_NONE] as $delta => $item): ?>
<?php $line_item = commerce_line_item_load($item['line_item_id']);?>
<tr class="odd"><td><?php print $line_item->line_item_label;?></td><td><?php print commerce_currency_format($line_item->commerce_unit_price[LANGUAGE_NONE][0]['amount'], $line_item->commerce_unit_price[LANGUAGE_NONE][0]['currency_code'])?></td><td><?php print $line_item->quantity;?></td><td><?php print commerce_currency_format($line_item->commerce_total[LANGUAGE_NONE][0]['amount'], $line_item->commerce_unit_price[LANGUAGE_NONE][0]['currency_code']); ?></td><td><?php print $status[$item['line_item_id']]; ?></td> </tr>
<?php endforeach; ?>
</tbody>
</table>
<?php $track_order_output = field_view_field('commerce_order', $order, 'commerce_customer_billing');
  print render($track_order_output);
?>

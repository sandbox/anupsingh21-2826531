README.txt
==========

Synopsis

This module is helpful for the site which uses e-commerce module, It provides
a graphical representation of state in which order currently is, to better
track the progress of the order placed.
In default module we have provided three basic styles for the track order
page, This can also be extended to include more skins by hook_skin_alter
based on the requirement of the site.
It has the feature to change the order status through bulk upload using
csv file. this upload can be done in one go or through Drupal queue
system to avoid server load.
Order status are tags in Drupal, so it can easily be extended for any
custom requirement.

Below is screen shot for default track order page

https://www.drupal.org/files/Track%20order.png

Requirements

This module requires following modules as dependencies-

Drupal commerce

Installation and Configuration:

Enable module through normal Drupal process

Go to Store -> Commerce track order -> Config and changes the number of order
to process in single cron, also you can changes the skin to apply on track
order page from either default or your custom skin(through code).

Go to Store -> Commerce track order(click) : this will take you to page where
you can bulk upload the order status by selecting "Process my orders later"
in case you want this to be processed through cron.

Go to Store -> Commerce track order -> Queue listing : here you can get the
list of pending status upload to be processed on cron.

INSTRUCTIONS
======================
1. Download and enable the module from module via /admin/modules or through
 drush command: drush en commerce_track_order
2. Admin users click on upload file link to upload order status file in
csv format
3. Site visitors can track their order
4. All results will be displayed graphically on the following page.



AUTHOR/MAINTAINER
======================
- As listed on module page
======================

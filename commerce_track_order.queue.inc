<?php

/**
 * @file
 * Processess the queue data.
 */

/**
 * Listing of Queue data.
 */
function commerce_track_order_queue_listing() {
  $items = commerce_track_order_retrieve_queue('queue_commerce_track_order');
  foreach ($items as &$item) {
    $item['created'] = date('r', $item['created']);
    $content = unserialize($item['data']);
    $item['order_id'] = $content[0];
    $item['status'] = $content[1];
    unset($item['data']);
    $rows[] = $item;
  }
  if (!empty($rows)) {
    $header = array(
      t('Item ID'),
      t('Created'),
      t('Order ID'),
      t('Status'),
    );
    $output = theme('table', array('header' => $header, 'rows' => $rows));
    return $output;
  }
  else {
    return t("No queues exist.");
  }
}

/**
 * Helper function to fetch Queue data.
 *
 * @param string $queue_name
 *   Returns $items.
 */
function commerce_track_order_retrieve_queue($queue_name) {
  $items = array();
  $result = db_query("SELECT item_id, data, created FROM {queue} WHERE name = :name ORDER BY item_id",
      array(':name' => $queue_name),
      array('fetch' => PDO::FETCH_ASSOC));
  foreach ($result as $item) {
    $items[] = $item;
  }
  return $items;
}

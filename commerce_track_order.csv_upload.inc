<?php

/**
 * @file
 * Provides CSV Upload form and funcationality.
 */

/**
 * Implements hook_form().
 */
function commerce_track_order_form($form, &$form_state) {
  $form['csv_file'] = array(
    '#type' => 'managed_file',
    '#title' => '',
    '#description' => t('Upload CSV File'),
    '#upload_location' => 'public://commerce_track_order_csv_files',
    '#attributes' => array('multiple' => 'multiple'),
    "#upload_validators"  => array("file_validate_extensions" => array("csv")),
  );
  $form['queue_orders'] = array(
    '#type' => 'checkbox',
    '#title' => 'Process my orders later',
  );
  $form['download_template_xml'] = array(
    '#type' => 'markup',
    '#markup' => '<div>' . l(t('Click Here'), 'commerce/csv/template') . ' To download CSV Template</div><br>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Upload CSV File',
    '#description' => t('Upload CSV File'),
  );
  return $form;
}

/**
 * Helper function to generate CSV file.
 */
function commerce_track_order_create_csv() {
  $filename = 'template.csv';

  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  header('Content-Description: File Transfer');
  header("Content-type: text/csv");
  header("Content-Disposition: attachment; filename={$filename}");
  header("Expires: 0");
  header("Pragma: public");

  $csv = array('orderid', 'status');

  $fh = @fopen('php://output', 'w');

  // Put the data into the stream.
  fputcsv($fh, $csv);
  fclose($fh);
  // Make sure nothing else is sent, our file is done.
  exit;
}

/**
 * Implements hook_form_submit().
 */
function commerce_track_order_form_submit($form, &$form_state) {
  $file_id = $form_state['values']['csv_file'];
  if (!$file_id) {
    return;
  }
  // Read uploaded csv file.
  $rows = commerce_track_order_read_csv($file_id);
  // rows[0] has the header row from csv.
  $headers = array_shift($rows);

  if ($form_state['values']['queue_orders'] == 1) {
    commerce_track_order_queue_orders($headers, $rows);
  }
  else {
    $totalcount = count($rows);
    $chunks = array_chunk($rows, 5);

    $batch = array(
      'title' => t('Uploading line item status'),
      'operations' => array(
        array(
          'commerce_track_order_batch',
          array($chunks, $totalcount),
        ),
      ),
      'progress_message' => t('Uploading line status'),
      'error_message' => t('Error!'),
      'finished' => 'commerce_track_order_batch_finish',
      'file' => drupal_get_path('module', 'commerce_track_order') . '/commerce_track_order.admin.inc',
    );
    batch_set($batch);
    // Only needed if not inside a form _submit handler.
    // Setting redirect in batch_process.
    commerce_track_order_delete_csv($file_id);
    batch_process('admin/structure/commerce_track_csv_upload');
  }
}

/**
 * Helper function for batch process.
 */
function commerce_track_order_batch($rows, $totalcount, &$context) {
  // Get Count of batch.
  if (empty($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = $totalcount;
  }
  // Count of operation in one batch step.
  $limit = count($rows) < 5 ? count($rows) : 5;

  $chunk = $context['sandbox']['progress'] / $limit;
  // Create Iteration variable.
  if (empty($context['sandbox']['iteration'])) {
    $context['sandbox']['iteration'] = 0;
  }

  if (is_array($rows)) {
    // Check for the end of cycle.
    if ($context['sandbox']['iteration'] < $context['sandbox']['max']) {
      // Counts completed operations in one batch step.
      $counter = 0;
      if ($context['sandbox']['progress'] != 0) {
        $context['sandbox']['iteration'] = $context['sandbox']['iteration'] + $limit;
      }
      foreach ($rows[$chunk] as $val) {
        $order_id = isset($val[0]) ? trim($val[0]) : 0;
        $status = isset($val[1]) ? trim($val[1]) : 0;

        $message = array();
        $added = array();
        if (!empty($order_id) && !empty($status)) {
          commerce_track_order_assign_track_status($status, $order_id);
        }
        else {
          $message[] = array('status' => FALSE, 'message_error' => 'Blank order row found.');
          $context['results']['message'][] = $message;
        }

        // Update Progress.
        $context['sandbox']['progress']++;
        $counter++;
        // Messages.
        $context['message'] = t('Now processing order %order. order %current of %count',
        array(
          '%order' => $order_id,
          '%current' => $context['sandbox']['progress'],
          '%count' => $context['sandbox']['max'],
        )
        );
        $context['results']['processed'] = $context['sandbox']['progress'];
        $context['results']['message'][] = $message;
        $context['results']['added'] = $added;
      }
    }
  }
  else {
    $message[] = array('status' => FALSE, 'message_error' => 'Excel sheet is blank, Please add some data');
    $context['results']['message'][] = $message;
  }

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Helper function for batch finish.
 */
function commerce_track_order_batch_finish($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('@count order status added.', array('@count' => isset($results['added']) ? count($results['added']) : 0)));
    foreach ($results['message'] as $message) {
      foreach ($message as $msg) {
        if ($msg['message_success']) {
          drupal_set_message($msg['message_success']);
        }
        if ($msg['message_error']) {
          drupal_set_message($msg['message_error'], 'error');
        }
      }
    }
  }
  else {
    $error_operation = reset($operations);
    drupal_set_message(
      t('An error occurred while processing @operation with arguments : @args',
        array(
          '@operation' => $error_operation[0],
          '@args' => print_r($error_operation[0], TRUE),
        )
      )
    );
  }
}

/**
 * Helper function to read csv file.
 */
function commerce_track_order_read_csv($fid) {
  $rows = array();
  $uri = db_query("SELECT uri FROM {file_managed} WHERE fid = :fid", array(':fid' => $fid))->fetchField();
  if ($uri) {
    $handle = fopen(drupal_realpath($uri), "r");
    if ($handle) {
      $rows[0] = fgetcsv($handle, 0, ',', '"');
      while (($data = fgetcsv($handle)) !== FALSE) {
        $rows[] = $data;
      }
    }
  }
  return ($rows);
}

/**
 * Helper function to delete csv file.
 */
function commerce_track_order_delete_csv($fid) {
  $uri = db_query("SELECT uri FROM {file_managed} WHERE fid = :fid", array(':fid' => $fid))->fetchField();

  db_delete('file_managed')
    ->condition('fid', $fid)
    ->execute();

  if (file_exists(drupal_realpath($uri))) {
    unlink(drupal_realpath($uri));
  }
}

/**
 * Add items to queue.
 */
function commerce_track_order_queue_orders($headers, $rows) {

  $queue = DrupalQueue::get('queue_commerce_track_order');
  $queue->createQueue();

  foreach ($rows as $row) {
    // Queue the string.
    $queue->createItem($row);
  }
  $count = $queue->numberOfItems();
  drupal_set_message(t('There are now @count items in the queue.', array('@count' => $count)));
}

<?php

/**
 * @file
 * Provides a Tracking feature of any order for Drupal commerce.
 */

/**
 * Defines the code for CSV file upload form for user interface.
 */

/**
 * Implements hook_menu().
 */
function commerce_track_order_menu() {
  $items['admin/commerce/commerce_track_order'] = array(
    'title' => 'Commerce track order',
    'description' => 'Commerce Track Order',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_track_order_form'),
    'access arguments' => array('commerce CSV upload'),
    'file' => 'commerce_track_order.csv_upload.inc',
  );
  $items['commerce/csv/template'] = array(
    'title' => 'Commerce track order CSV',
    'description' => 'Commerce Track Order CSV',
    'page callback' => 'commerce_track_order_create_csv',
    'access arguments' => array('commerce CSV upload'),
    'file' => 'commerce_track_order.csv_upload.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/commerce/commerce_track_order/status_upload'] = array(
    'title' => 'Status upload',
    'description' => 'Commerce Track Order',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/commerce/commerce_track_order/queue'] = array(
    'title' => 'Queue Listing',
    'page callback' => 'commerce_track_order_queue_listing',
    'access arguments' => array('commerce track your order'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'commerce_track_order.queue.inc',
  );
  $items['admin/commerce/commerce_track_order/conf'] = array(
    'title' => 'Configure',
    'description' => 'Configure commerce track',
    'page arguments' => array('commerce_track_order_configuration'),
    'access arguments' => array('commerce CSV upload'),
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/commerce/orders/%commerce_order/track'] = array(
    'title' => 'Track Order',
    'page callback' => 'commerce_track_order_menu_item_redirect',
    'page arguments' => array(3),
    'access arguments' => array('commerce track your order'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 21,
    'file' => 'commerce_track_order.admin.inc',
  );
  $items['user/%user/orders/%commerce_order/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['user/%user/orders/%commerce_order/track'] = array(
    'title' => 'Track Your Order',
    'page callback' => 'commerce_track_order_menu_item_redirect',
    'page arguments' => array(3, 1),
    'access arguments' => array('commerce track your order'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'commerce_track_order.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function commerce_track_order_permission() {
  return array(
    'commerce CSV upload' => array(
      'title' => t('Administer commerce CSV upload'),
    ),
    'commerce track your order' => array(
      'title' => t('Access to commerce track your order'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function commerce_track_order_theme($existing, $type, $theme, $path) {
  $module_path = drupal_get_path('module', 'commerce_track_order');
  return array(
    'commerce_track_order' => array(
      'variables' => array('order' => NULL, 'status' => NULL),
      'template' => 'commerce-track-order',
      'path' => $module_path . '/templates',
    ),
    'commerce_track_order_grid' => array(
      'variables' => array('order' => NULL, 'status' => NULL),
      'template' => 'commerce-track-order-grid',
      'path' => $module_path . '/templates',
    ),
    'commerce_track_order_interactive' => array(
      'variables' => array('order' => NULL, 'status' => NULL),
      'template' => 'commerce-track-order-interactive',
      'path' => $module_path . '/templates',
    ),
  );
}

/**
 * Helper function to get order status detail.
 */
function commerce_track_order_all_order_status() {
  $track_vocab = taxonomy_vocabulary_machine_name_load('commerce_order_track_status');
  $query = db_select('taxonomy_term_data', 'ttd')
    ->fields('ttd', array('name', 'tid', 'weight'))
    ->condition('ttd.vid', $track_vocab->vid)
    ->orderBy('ttd.weight', 'ASC')
    ->execute();
  $order_status = $query->fetchAll();
  return $order_status;
}

/**
 * Helper function to assign order status to order.
 */
function commerce_track_order_assign_track_status($status, $order_id) {
  $order = commerce_order_load($order_id);
  if (!empty($order)) {
    $status_tid = commerce_track_order_tid_from_machine_name($status);
    // Call custom hook.
    module_invoke_all('commerce_track_order_status_presave', $order);
    $order->field_order_status[LANGUAGE_NONE][0]['tid'] = $status_tid;
    commerce_order_save($order);
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Helper function to get term tids.
 */
function commerce_track_order_tid_from_machine_name($machine_name) {
  $query = db_select('field_data_field_order_status_machine_name', 'f')
    ->fields('f', array('entity_id'))
    ->condition('f.field_order_status_machine_name_value', $machine_name)
    ->execute();
  $tid = $query->fetchfield();
  return $tid;
}

/**
 * Implements hook_commerce_order_presave().
 *
 * Change order status to "Approval" when internal status is "complete".
 */
function commerce_track_order_commerce_order_presave($order) {
  if ($order->status == 'completed') {
    $status = commerce_track_order_all_order_status();
    $order->field_order_status[LANGUAGE_NONE][0]['tid'] = $status[0]->tid;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function commerce_track_order_form_taxonomy_form_term_alter(&$form, &$form_state, $form_id) {
  $form['field_order_status_machine_name']['#element_validate'][] = 'commerce_track_order_validate_taxonomy_machine_name';
}

/**
 * Validation for taxonomy machine name save.
 */
function commerce_track_order_validate_taxonomy_machine_name($element, &$form_state, $form) {
  $defult_value = $element[LANGUAGE_NONE][0]['value']['#default_value'];
  $machine_name = $form_state['values']['field_order_status_machine_name'][LANGUAGE_NONE][0]['value'];
  $query = db_select('field_data_field_order_status_machine_name', 'm')
    ->fields('m', array('field_order_status_machine_name_value'))
    ->condition('m.field_order_status_machine_name_value', $machine_name)
    ->condition('m.bundle', 'commerce_order_track_status')
    ->condition('m.entity_type', 'taxonomy_term');
  $stored_name = $query->execute()->fetchField();
  if (empty($stored_name) && !preg_match("/^[0-9a-z\_]*$/", $machine_name)) {
    form_error($element, t("The machine-readable name must contain only lowercase letters, numbers, and underscores."));
  }
  if (!empty($stored_name) && $stored_name !== $defult_value) {
    form_error($element, t("The machine-readable name already exist."));
  }
}

/**
 * Implements hook_cron().
 */
function commerce_track_order_cron() {
  $claim_limit = variable_get('commerce_track_order_cron_size', '25');
  // claim, process and delete items from queue.
  $queue = DrupalQueue::get('queue_commerce_track_order');
  // There is no harm in trying to recreate existing.
  $queue->createQueue();
  $count = $queue->numberOfItems();
  // Get 5 items from queue.
  for ($i = 0; $i < $claim_limit; $i++) {
    $queue_item = $queue->claimItem(60);
    if (!$queue_item) {
      // Blank item.
      break;
    }
    $item[$i] = $queue_item;
    $queue->deleteItem($queue_item);
  }
  if (!empty($item)) {
    foreach ($item as $val) {
      $order_id = $val->data[0];
      $status = $val->data[1];
      if (!empty($order_id) && !empty($status)) {
        commerce_track_order_assign_track_status($status, $order_id);
      }
      else {
        watchdog('commerce_track_order', 'Blank row found');
      }
    }
    $count = $queue->numberOfItems();
    watchdog('commerce_track_order', 'There are now @count items in the queue.', array('@count' => $count));
  }
  else {
    $count = $queue->numberOfItems();
    watchdog('commerce_track_order', 'There were no items in the queue available to claim/delete. There are currently @count items in the queue.', array('@count' => $count));
  }

}

/**
 * Implements hook_help().
 */
function commerce_track_order_help($path, $arg) {
  switch ($path) {
    case 'admin/help#commerce_track_order':
      $output = '<h3>' . t('Commerce Track Order') . '</h3>';
      $output .= '<p>' . t('A module providing a graphical representation of order status.') . '</p>';
      $output .= '<p>' . t('This module is very useful for sites with ecommerce. It will be helpful for users who need to track multiple orders.') . '</p>';
      $output .= '<p>' . t('Commerce track Order module provides a CSV uploder for commerce orders. The functionality can be found at  <a href="@csvurl">Commerce Track CSV upload</a> administration page.', array('@csvurl' => url('admin/structure/commerce_track_csv_upload'))) . '</p>';
      return $output;
  }
}

/**
 * Configuration form.
 *
 * @return form
 *   Retuens settings form.
 */
function commerce_track_order_configuration() {
  $form = array();
  $form['commerce_track_order_cron_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of items to be processed in each cron run'),
    '#default_value' => variable_get('commerce_track_order_cron_size', '25'),
    '#required' => TRUE,
  );

  $skin_list = array(
    'commerce_track_order' => 'Normal',
    'commerce_track_order_grid' => 'Grid',
    'commerce_track_order_interactive' => 'Interactive',
  );
  module_invoke_all('commerce_track_order_skin_alter', $skin_list);

  $form['commerce_track_order_skin'] = array(
    '#type' => 'select',
    '#title' => t('Select a skin from list'),
    '#options' => $skin_list,
    '#default_value' => variable_get('commerce_track_order_skin', 'commerce_track_order'),
    '#required' => TRUE,
  );

  return system_settings_form($form);

}

/**
 * Hook_validate.
 */
function commerce_track_order_configuration_validate($form, $form_state) {
  if (!is_numeric($form_state['values']['commerce_track_order_cron_size'])) {
    form_set_error('commerce_track_order_cron_size', t('Please enter integer value'));
  }
}
